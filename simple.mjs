// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
 
import RegExp2Rust from "./index.ts";

function build(regex,flag){
    try {
        let reg = new RegExp(regex,flag);
        let program = new RegExp2Rust(reg).build();
        console.log("1");
        console.log(program);
    }
    catch (e){
        console.log("0");
        console.log(e);
    }
}

global.build=build;