// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const cfc = require('@unicode/unicode-15.0.0/Case_Folding/C/code-points');
const cfs = require('@unicode/unicode-15.0.0/Case_Folding/S/code-points');

const cflist = Array.from(cfc.entries()).concat(Array.from(cfs.entries()));

const propertyAliases = require('unicode-canonical-property-names-ecmascript');
const propertyValueAliases = require('unicode-match-property-value-ecmascript/data/mappings.js');

const data = {};
const x = require(`@unicode/unicode-15.0.0`);


for(let [property,valueMap] of propertyValueAliases ){
    for (let [_,value] of valueMap){
        if ((property=="Script")&&(value == "Katakana_Or_Hiragana" )) {
            continue;
        }
        if ((property=="Script_Extensions")&&(value == "Katakana_Or_Hiragana" )) {
            continue;
        }
        if ((property=="Script")&&(value == "Unknown" )) {
            continue;
        }
        if ((property=="Script_Extensions")&&(value == "Unknown" )) {
            continue;
        }
        data[`${property}=${value}`] = conv(require(`@unicode/unicode-15.0.0/${property}/${value}/ranges.js`));
    }
}

for(let value of propertyAliases) {
    if (["Script_Extensions","Script","General_Category"].includes(value)) continue;
    data[`${value}`] = conv(require(`@unicode/unicode-15.0.0/Binary_Property/${value}/ranges.js`));
}

function conv(src){
    return src.map(src=>[src.begin,src.end-1]);
}


const propertyValueAliases2 = require("unicode-property-value-aliases-ecmascript");
const propertyAliases2 = require("unicode-property-aliases-ecmascript");

function mapKV(map){
    let list = [];
    for (let [key,value] of map.entries()) {
        list.push(`["${key}","${value}"]`);
    }
    return `new Map([${list.join(",")}])`;
}

function mapKVV(map){
    let list = [];
    for (let [key,value] of map.entries()) {
        list.push(`["${key}",${mapKV(value)}]`);
    }
    return `new Map([${list.join(",")}])`;
}

/// missing Script=Unknown and Script_Extensions=Unknown
/// regexpp missing too

const fs = require("fs");
let program = `
const data : Record<string,[number,number][]> = ${JSON.stringify(data)};
export default data;
export const caseFold : [number,number][] = ${JSON.stringify(cflist)};
export const propertyValueAliases : Map<string,Map<string,string>> = ${mapKVV(propertyValueAliases2)};
export const propertyAliases : Map<string,string> = ${mapKV(propertyAliases2)};
`
fs.writeFileSync('./unicode.ts', program, 'utf8');