// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


import {
    AST,
    RegExpParser,
    RegExpValidator,
    parseRegExpLiteral,
    validateRegExpLiteral,
    visitRegExpAST,
} from "regexpp";

import unicode from "./unicode";
import { caseFold, propertyValueAliases, propertyAliases } from "./unicode"
import * as utf8parser from "@protobufjs/utf8";

let fold: Map<number, number[]> = new Map();

for (let item of caseFold) {
    if (!fold.has(item[1])) {
        fold.set(item[1], []);
    }
    fold.get(item[1]).push(item[0]);
    if (!fold.has(item[0])) {
        fold.set(item[0], []);
    }
    fold.get(item[0]).push(item[1]);
}

type Global = {
    l2r: boolean,
}

export default class RegExp2Rust {
    ast: AST.RegExpLiteral
    pattern: AST.Pattern
    offset2id: Record<number, number>;
    group2id: Record<string, number>;
    groupenumstr: string;
    count: number;
    constructor(regex: RegExp) {
        this.ast = parseRegExpLiteral(regex);
        this.pattern = this.ast.pattern;
        [this.offset2id, this.group2id, this.count] = buildCapturingGroupIDS(this.ast);
        this.groupenumstr = buildGroupEnum(this.group2id);
        let flags = this.ast.flags;
        if (!flags.unicode) {
            throw "flags missing unicode(u)";
        }
        if (flags.hasIndices) {
            throw "flags hasIndices(d) not impl";
        }
    }
    build() {
        let f: string[] = [];
        let iter = "";
        if (this.ast.flags.global) {
            iter = this.genIter();
        }
        let isGlobal = this.ast.flags.global;
        let isSticky = this.ast.flags.sticky;
        let isAssertStart = !this.ast.flags.multiline;
        if (isAssertStart) {

            for (let alt of this.ast.pattern.alternatives) {
                let first = alt.elements[0];
                if (first.type != "Assertion" || first.kind != "start") {
                    isAssertStart = false;
                    break;
                }
            }
        }
        let lastIndexParam = "";
        let lastIndexDefine = "let mut last_index = 0usize;";
        if (isSticky || isGlobal) {
            lastIndexParam = ",mut last_index:usize";
            lastIndexDefine = "";
        }
        let whileStmt = `while last_index<=s.len() {`;
        let utf8With = `
        if last_index==s.len() {
            last_index=usize::MAX;
        }
        else {
            last_index=last_index + (UTF8_CHAR_WIDTH[s.as_bytes()[last_index] as usize] as usize);
        }
    }`;
        let enableMemchr: number[] = [];
        if (isSticky || isAssertStart) {
            whileStmt = "";
            utf8With = "";
        }
        else {
            let crange = CharRanges.empty();
            for (let alt of this.ast.pattern.alternatives) {
                let first = alt.elements[0];
                if (first.type == "Character") {
                    crange.union(CharRanges.fromCharacter(first));
                    continue;
                }
                if (first.type == "CharacterClass") {
                    crange.union(CharRanges.fromCharacterClass(first, this.ast.flags.dotAll));
                    continue;
                }
                if (first.type == "CharacterSet") {
                    crange.union(CharRanges.fromCharacterSet(first, this.ast.flags.dotAll));
                    continue;
                }
            }
            if (this.ast.flags.ignoreCase) {
                crange.unCaseFold();
            }
            if (crange.onlyAscii()) {
                let list = crange.charCodeList();
                if (list.length <= 3) {
                    enableMemchr = list;
                }
            }
        }
        let alternatives = this.pattern.alternatives;
        let returnValue = `[None;${this.count + 1}]`;
        let memchrStmt;
        [memchrStmt, alternatives] = this.genMemchr(enableMemchr, alternatives, f);
        let altscode = "";
        if (((alternatives.length != 1) || (alternatives[0].elements.length != 0)) && (alternatives.length != 0)) {
            let falts = newf(f);
            this.genAlts(alternatives, f, { l2r: true }, falts, null);
            altscode = `
            if ${fn(falts)}(s,&mut end,&mut matches) {
                matches[0]=Some([last_index,end]);
                return matches;
            }
        `;
        }
        else {
            whileStmt = "";
            utf8With = "";
            returnValue = "";
        }
        return `

        // https://tools.ietf.org/html/rfc3629
        const UTF8_CHAR_WIDTH: &[u8; 256] = &[
            // 1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 0
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 1
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 2
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 3
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 4
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 5
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 6
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 7
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 8
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 9
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // A
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // B
            0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // C
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // D
            3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // E
            4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // F
        ];

        ${f.join("")}

        ///${this.pattern.raw}
        #[allow(unused)]
        #[inline]
        pub fn exec(s:&str${lastIndexParam})->[Option<[usize;2]>;${this.count + 1}]{
            ${lastIndexDefine}
            ${whileStmt}
                let mut end = last_index;
                let mut matches : [Option<[usize;2]>;${this.count + 1}] = [None;${this.count + 1}];
                ${memchrStmt}
                ${altscode}
            ${utf8With}
            ${returnValue}
        }
        #[allow(unused)]
        #[derive(Copy,Clone,Debug)]
        pub enum Group{
${this.groupenumstr}
        }
        ${iter}

        #[allow(unused)]
        #[inline]
        pub fn matches(s:&str${(isSticky || isGlobal) ? ",last_index:usize" : ""})->[Option<&str>;${this.count + 1}]{
            exec(s${(isSticky || isGlobal) ? ",last_index" : ""}).map(|v|match v{
                Some([start,end])=>Some(&s[start..end]),
                None=>None,
            })
        }
        #[allow(unused)]
        pub struct Matches<'a>(&'a str,[Option<[usize;2]>;${this.count + 1}]);
        #[allow(unused)]
        pub fn typed_matches(s:&str${(isSticky || isGlobal) ? ",last_index:usize" : ""})->Matches<'_>{
            let matched = exec(s${(isSticky || isGlobal) ? ",last_index" : ""});
            Matches(s,matched)
        }
        #[allow(unused)]
        impl<'a> Matches<'a>{
            pub fn get_str(&self,group:Group)->Option<&'a str>{
                let range = self.get_range(group)?;
                Some(&self.0[range[0]..range[1]])
            }
            pub fn get_range(&self,group:Group)->Option<[usize;2]>{
                self.1[group as usize]
            }
            pub fn get_str_id(&self,id:usize)->Option<&'a str>{
                let range = self.get_range_id(id)?;
                Some(&self.0[range[0]..range[1]])
            }
            pub fn get_range_id(&self,id:usize)->Option<[usize;2]>{
                if id>=self.1.len() {
                    return None;
                }
                self.1[id]
            }
        }
`
    }
    genMemchr(enableMemchr: number[], alts: AST.Alternative[], f: string[]): [string, AST.Alternative[]] {
        //let ac = this.genAc(alts);
        let ac = this.genAC(alts, f);
        if (ac != null) {
            return [ac, []]
        }
        if (enableMemchr.length == 1) {
            let s = "";
            let newAlts = alts.map((alt) => {
                let newAlt = Object.assign({}, alt);
                newAlt.elements = [];
                return newAlt;
            })
            let id = 0;
            for (; id < alts[0].elements.length; id++) {
                if (alts[0].elements[id].type == "Character" && alts.every(value => value.elements[id]?.raw == alts[0].elements[id].raw)) {
                    s = s + alts[0].elements[id].raw;
                }
                else {
                    break;
                }
            }
            newAlts.forEach((newAlt, altid) => {
                newAlt.elements = alts[altid].elements.slice(id);
            })
            let return_true = "";
            if (newAlts.length == 1 && newAlts[0].elements.length == 0) {
                return_true = `
                        matches[0]=Some([last_index,end]);
                        return matches;
                `
            }
            let news = `
                match memchr::memmem::find(&s.as_bytes()[end..], b"${s}"){
                    Some(offset)=>{
                        last_index = end+offset;
                        end = last_index+b"${s}".len();
                        ${return_true}
                    },
                    None=>return [None;${this.count + 1}],
                }`;
            return [news, newAlts];
        }
        if ((enableMemchr.length == 2 || enableMemchr.length == 3)) {
            let newAlts = alts.map((alt) => {
                let newAlt = Object.assign({}, alt);
                return newAlt;
            });
            let n = "";
            if (alts.every(value => value.elements[0].raw == alts[0].elements[0].raw)) {
                n = "+1";
                newAlts.forEach((newAlt, altid) => {
                    newAlt.elements = alts[altid].elements.slice(1);
                })
            }

            let memchrCall = enableMemchr.length == 2 ?
                `memchr::memchr2( ${enableMemchr[0]}, ${enableMemchr[1]}, &s.as_bytes()[end..])` :
                `memchr::memchr3( ${enableMemchr[0]}, ${enableMemchr[1]}, ${enableMemchr[2]}, &s.as_bytes()[end..])`;
            let return_true = "";
            if (newAlts.length == 1 && newAlts[0].elements.length == 0) {
                return_true = `
                            matches[0]=Some([last_index,end]);
                            return matches;
                    `
            }
            let news = `
                match ${memchrCall}{
                    Some(offset)=>{
                        last_index = end+offset;
                        end = last_index${n};
                        ${return_true}
                    },
                    None=>return [None;${this.count + 1}],
                }`;

            return [news, newAlts];
        }
        return ["", alts];
    }
    genAC(alts: AST.Alternative[], f: string[]): string | null {
        if (this.ast.flags.sticky == true) {
            return null;
        }
        let ac = buildTrieWithFail(alts, this.ast.flags);
        if (ac == null) {
            return null;
        }
        let init = "while last_index<=s.len() {";
        let memchr = "";
        let memchrend = "";
        let keys = Object.keys(ac.subtries);
        let prefixlen = 0;
        let patMinLen = minLen(ac, 0);
        let patList = pats(ac, []);
        let avx2 = false;
        if (patList.length == 1) {
            return null;
        }
        else if (patMinLen >= 2 && patList.length < 64) {
            let hashLen = patMinLen;
            let hash2pow = Math.pow(2, hashLen - 1);
            if (patMinLen >= 4 && patList.length < 64) {
                avx2 = true;
            }
            init = `
            const fn hash( bytes: &[u8]) -> usize {
                let mut hash = 0usize;
                let mut n = 0;
                while n<bytes.len() {
                    hash = hash.wrapping_shl(1).wrapping_add(bytes[n] as usize);
                    n+=1;
                }
                hash
            }
            const BLK_LENS : usize = 64;
            const fn init_rabin_karp() -> [bool;BLK_LENS] {
                let mut blks = [false;BLK_LENS];
                let pat_list:[&'static [u8];${patList.length}] = [
                    ${patList.map(v => `&[${v.join(",")}]`).join("\n,")}
                ];
                let mut i = 0;
                while i < pat_list.len() {
                    blks[hash(pat_list[i]) % BLK_LENS]=true;
                    i+=1;
                }
                blks
            }
            const HASH2POW : usize = ${hash2pow};
            const RK : [bool;BLK_LENS] = init_rabin_karp();
            fn update_hash(prev: usize, old_byte: u8, new_byte: u8) -> usize {
                prev.wrapping_sub((old_byte as usize).wrapping_mul(HASH2POW))
                    .wrapping_shl(1)
                    .wrapping_add(new_byte as usize)
            }

            let mut hval = hash(&s.as_bytes()[last_index..last_index + ${hashLen}]);
            loop {
            `;
            memchr = `
                if RK[hval%BLK_LENS] {
        
            `;
            memchrend = `
                }
                if last_index + ${hashLen} >= s.len() {
                    break;
                }
                hval = update_hash(
                    hval,
                    s.as_bytes()[last_index],
                    s.as_bytes()[last_index + ${hashLen}],
                );
            `

        } else if (keys.length == 1) {
            init = "while last_index<=s.len() {";
            memchr = `
            match memchr::memchr(${keys[0]}, &s.as_bytes()[last_index..]) {
                Some(n) => last_index=last_index+n,
                None=>break,
            }
            `;
            ac = ac.subtries[keys[0]];
            prefixlen = 1;
        } else if (keys.length == 2) {
            init = "while last_index<=s.len() {";
            memchr = `
            match memchr::memchr2(${keys[0]}, ${keys[1]}, &s.as_bytes()[last_index..]) {
                Some(n) => last_index=last_index+n,
                None=>break,
            }
            `;
        } else if (keys.length == 3) {
            init = "while last_index<=s.len() {";
            memchr = `
            match memchr::memchr3(${keys[0]}, ${keys[1]}, ${keys[2]}, &s.as_bytes()[last_index..]) {
                Some(n) => last_index=last_index+n,
                None=>break,
            }
            `;
        }
        let cache = {};
        let [id, notCache] = newfWithCache(f, [], cache);
        if (notCache) {
            this.genSubAC(ac, f, id, [], ac, "", cache, prefixlen);
        }
        let code = `
        ${init}
            ${memchr}
            if let Some((start,end))=${fn(id)}(s,last_index+${prefixlen}) {
                matches[0]=Some([start,end]);
                return matches;
            }
            ${memchrend}
            last_index+=1;
        }
        [None;${this.count + 1}]

        `;
        if (avx2) {
            let blklen = 16;
            let masklen = 4;
            let masks: [number[], number[]][] = new Array(4).fill(0).map(_ => [new Array(32).fill(0), new Array(32).fill(0)]);
            let buckets: number[][] = new Array(blklen).fill(0).map(_ => []);
            let lonibble_to_bucket: Record<string, number> = {};
            for (let id in patList) {
                let lonybs = patList[id].slice(0, 4).map(b => b & 0xF);
                let bucket;
                if (lonibble_to_bucket[lonybs.toString()] == undefined) {
                    bucket = (blklen - 1) - (parseInt(id) % blklen);
                    lonibble_to_bucket[lonybs.toString()] = bucket;
                }
                else {
                    bucket = lonibble_to_bucket[lonybs.toString()];
                }
                buckets[bucket].push(parseInt(id));

            }
            function add_fat(mask: [number[], number[]], bucket: number, byte: number) {
                let byte_lo = (byte & 0xF);
                let byte_hi = ((byte >> 4) & 0xF);
                mask[0][byte_lo + 16] |= 1 << (bucket % 8);
                mask[1][byte_hi + 16] |= 1 << (bucket % 8);
            }
            for (let bid in buckets) {
                let bucket = buckets[bid];
                for (let pat_id of bucket) {
                    let pat = patList[pat_id];
                    for (let i in masks) {
                        let mask = masks[i];
                        add_fat(mask, parseInt(bid), pat[i]);
                    }
                }
            }
            let blks = `[${buckets.map(bucket => `&[${bucket.map(n => n.toString()).join(",")}]`).join(",")}]`;
            let maskss = `[${masks.map(mask => `(${mask.map(n => `[${n.join(",")}]`).join(",")})`).join(",")}]`;
            code = `
            thread_local! { 
                static ENABLEX86 : bool = is_x86_feature_detected!("sse2") && is_x86_feature_detected!("avx2");
            }
            #[cfg(all(target_arch = "x86_64"))]
            #[target_feature(enable = "sse2,avx2")]
            unsafe fn teddy_fat(s:&str,last_index:usize)->Option<[usize;2]>{
                unsafe {
                    use core::arch::x86_64::*;
                    const PATS: [&'static [u8];${patList.length}] = [${patList.map(p => `&[${p.join(",")}]`)}];
                    const BLKS: [&'static [usize];${blklen}] = ${blks};
                    const MASKS: [([u8;32],[u8;32]); ${masklen}] = ${maskss};

                    unsafe fn members4m256(
                        chunk: __m256i,
                        mask1: (__m256i,__m256i),
                        mask2: (__m256i,__m256i),
                        mask3: (__m256i,__m256i),
                        mask4: (__m256i,__m256i),
                    ) -> (__m256i, __m256i, __m256i, __m256i) {
                        let lomask = _mm256_set1_epi8(0xF);
                        let hlo = _mm256_and_si256(chunk, lomask);
                        let hhi = _mm256_and_si256(_mm256_srli_epi16(chunk, 4), lomask);
                        let res0 = _mm256_and_si256(
                            _mm256_shuffle_epi8(mask1.0, hlo),
                            _mm256_shuffle_epi8(mask1.1, hhi),
                        );
                        let res1 = _mm256_and_si256(
                            _mm256_shuffle_epi8(mask2.0, hlo),
                            _mm256_shuffle_epi8(mask2.1, hhi),
                        );
                        let res2 = _mm256_and_si256(
                            _mm256_shuffle_epi8(mask3.0, hlo),
                            _mm256_shuffle_epi8(mask3.1, hhi),
                        );
                        let res3 = _mm256_and_si256(
                            _mm256_shuffle_epi8(mask4.0, hlo),
                            _mm256_shuffle_epi8(mask4.1, hhi),
                        );
                        (res0, res1, res2, res3)
                    }
                    
                    pub unsafe fn loadu128(slice: &[u8], at: usize) -> __m128i {
                        let ptr = slice.get_unchecked(at..).as_ptr();
                        _mm_loadu_si128(ptr as *const u8 as *const __m128i)
                    }
                    
                    pub unsafe fn is_all_zeroes256(a: __m256i) -> bool {
                        let cmp = _mm256_cmpeq_epi8(a, _mm256_set1_epi8(0));
                        _mm256_movemask_epi8(cmp) as u32 == 0xFFFFFFFF
                    }
                    #[inline(always)]
                    pub unsafe fn unpacklo64x256(a: __m256i, b: __m256i) -> [u64; 4] {
                        let lo = _mm256_castsi256_si128(a);
                        let hi = _mm256_castsi256_si128(b);
                        [
                            _mm_cvtsi128_si64(lo) as u64,
                            _mm_cvtsi128_si64(_mm_srli_si128(lo, 8)) as u64,
                            _mm_cvtsi128_si64(hi) as u64,
                            _mm_cvtsi128_si64(_mm_srli_si128(hi, 8)) as u64,
                        ]
                    }
                    #[inline(always)]
                    fn verify_bucket(
                        haystack: &[u8],
                        bucket: usize,
                        at: usize,
                    ) -> Option<[usize;2]> {
                
                        for &pati in BLKS[bucket] {
                            let pat = PATS[pati];
                            if pat.len() <= (haystack.len()-at) && pat == &haystack[ at .. at + pat.len() ] {
                                return Some([at, at + pat.len()]);
                            }
                        }
                        None
                    }
                    #[inline(always)]
                    fn verify64(
                        haystack: &[u8],
                        at: usize,
                        mut cand: u64,
                    ) -> Option<[usize;2]> {
                        while cand != 0 {
                            let bit = cand.trailing_zeros() as usize;
                            cand &= !(1 << bit);
                
                            let at = at + (bit / ${blklen});
                            let bucket = bit % ${blklen};
                            if let Some(m) = verify_bucket(haystack, bucket, at) {
                                return Some(m);
                            }
                        }
                        None
                    }
                    #[inline(always)]
                    pub fn mask2m256i(mask: ([u8;32],[u8;32])) -> (__m256i,__m256i) {
                        // SAFETY: This is safe since [u8; 32] has the same representation
                        // as __m256i.
                        unsafe {
                            (
                                core::mem::transmute(mask.0),
                                core::mem::transmute(mask.1),
                            )
                        }
                    }
                    #[inline(always)]
                    unsafe fn verify_fat256(
                        haystack: &[u8],
                        at: usize,
                        cand: __m256i,
                    ) -> Option<[usize;2]> {

                        // This is a bit tricky, but we basically want to convert our
                        // candidate, which looks like this
                        //
                        //     a31 a30 ... a17 a16 a15 a14 ... a01 a00
                        //
                        // where each a(i) is an 8-bit bitset corresponding to the activated
                        // buckets, to this
                        //
                        //     a31 a15 a30 a14 a29 a13 ... a18 a02 a17 a01 a16 a00
                        //
                        // Namely, for Fat Teddy, the high 128-bits of the candidate correspond
                        // to the same bytes in the haystack in the low 128-bits (so we only
                        // scan 16 bytes at a time), but are for buckets 8-15 instead of 0-7.
                        //
                        // The verification routine wants to look at all potentially matching
                        // buckets before moving on to the next lane. So for example, both
                        // a16 and a00 both correspond to the first byte in our window; a00
                        // contains buckets 0-7 and a16 contains buckets 8-15. Specifically,
                        // a16 should be checked before a01. So the transformation shown above
                        // allows us to use our normal verification procedure with one small
                        // change: we treat each bitset as 16 bits instead of 8 bits.

                        // Swap the 128-bit lanes in the candidate vector.
                        let swap = _mm256_permute4x64_epi64(cand, 0x4E);
                        // Interleave the bytes from the low 128-bit lanes, starting with
                        // cand first.
                        let r1 = _mm256_unpacklo_epi8(cand, swap);
                        // Interleave the bytes from the high 128-bit lanes, starting with
                        // cand first.
                        let r2 = _mm256_unpackhi_epi8(cand, swap);
                        // Now just take the 2 low 64-bit integers from both r1 and r2. We
                        // can drop the high 64-bit integers because they are a mirror image
                        // of the low 64-bit integers. All we care about are the low 128-bit
                        // lanes of r1 and r2. Combined, they contain all our 16-bit bitsets
                        // laid out in the desired order, as described above.
                        let parts = unpacklo64x256(r1, r2);
                        for (i, &part) in parts.iter().enumerate() {
                            let pos = at + i * 4;
                            if let Some(m) = verify64(haystack, pos, part) {
                                return Some(m);
                            }
                        }
                        None
                    }

                    #[inline(always)]
                    unsafe fn candidate(
                        haystack: &[u8],
                        at: usize,
                        prev0: &mut __m256i,
                        prev1: &mut __m256i,
                        prev2: &mut __m256i,
                    ) -> __m256i {
                        let chunk =
                            _mm256_broadcastsi128_si256(loadu128(haystack, at));
                        let (res0, res1, res2, res3) = members4m256(
                            chunk, mask2m256i(MASKS[0]), mask2m256i(MASKS[1]), mask2m256i(MASKS[2]), mask2m256i(MASKS[3]),
                        );
                        let res0prev0 = _mm256_alignr_epi8(res0, *prev0, 13);
                        let res1prev1 = _mm256_alignr_epi8(res1, *prev1, 14);
                        let res2prev2 = _mm256_alignr_epi8(res2, *prev2, 15);
                        let res = _mm256_and_si256(
                            _mm256_and_si256(
                                _mm256_and_si256(res0prev0, res1prev1),
                                res2prev2,
                            ),
                            res3,
                        );
                        *prev0 = res0;
                        *prev1 = res1;
                        *prev2 = res2;
                        res
                    }
                    let haystack = s.as_bytes();
                    let mut at = last_index;
                    at+=3;
                    let len = haystack.len();

                    let mut prev0 = _mm256_set1_epi8(0xFF as u8 as i8);
                    let mut prev1 = _mm256_set1_epi8(0xFF as u8 as i8);
                    let mut prev2 = _mm256_set1_epi8(0xFF as u8 as i8);
                    while at <= len - 16 {
                        let c = candidate(haystack, at, &mut prev0, &mut prev1, &mut prev2);
                        if !is_all_zeroes256(c) {
                            if let Some(m) = verify_fat256(haystack, at - 3, c)
                            {
                                return Some(m);
                            }
                        }
                        at += 16;
                    }
                    if at < len {
                        at = len - 16;
                        prev0 = _mm256_set1_epi8(0xFF as u8 as i8);
                        prev1 = _mm256_set1_epi8(0xFF as u8 as i8);
                        prev2 = _mm256_set1_epi8(0xFF as u8 as i8);
            
                        let c = candidate(haystack, at, &mut prev0, &mut prev1, &mut prev2);
                        if !is_all_zeroes256(c) {
                            if let Some(m) = verify_fat256(haystack, at - 3, c)
                            {
                                return Some(m);
                            }
                        }
                    }
                    None
                }
            }
            if s.len()-last_index>=19 && ENABLEX86.with(|cell| *cell) {
                return [unsafe {teddy_fat(s,last_index)};${this.count + 1}];
            }
            {
                ${code}
            }
            `
        }
        return code;
    }
    genSubAC(ac: Trie, f: string[], id: number, prefix: number[], root: Trie, tag: string, cache: Record<string, number>, prefixlen: number) {
        if (Object.keys(ac.subtries).length == 0) {
            f[id] = `
        //ac ${prefix.join(",")} ${tag}
        
        fn ${fn(id)}(s:&str,offset:usize)->Option<(usize,usize)>{
            return Some(offset-${prefix.length + prefixlen},offset)
        }
            `
        }
        let cases = [];
        for (let key in ac.subtries) {
            let subtrie = ac.subtries[key];
            let [subfid, notCache] = newfWithCache(f, prefix.concat(parseInt(key)), cache);
            if (notCache) {
                let newprefix = prefix.concat(parseInt(key));
                this.genSubAC(subtrie, f, subfid, newprefix, root, newprefix.toString(), cache, prefixlen);
            }
            cases.push(`
            ${key} => {
                if let Some(range)=${fn(subfid)}(s,offset+1) {
                    return Some(range);
                }
            }
            `)
        }
        let prefix2 = Array.from(prefix);
        let returnStmt = "None";

        let failPtr = ac.failPtr.slice(prefixlen);
        if (failPtr.length != 0) {
            let failtarget = getSubTrie(root, failPtr);
            let [failfid, notCache] = newfWithCache(f, failPtr, cache);
            if (notCache) {
                this.genSubAC(failtarget, f, failfid, failPtr, root, ac.failPtr[ac.failPtr.length - 1].toString(), cache, prefixlen);
            }
            returnStmt = `
            return ${fn(failfid)}(s,offset);
            `
        }

        while (true) {
            if (getSubTrie(root, prefix2).power == 1) {
                returnStmt = `
            Some((offset-${prefix.length + prefixlen},offset-${prefix.length - prefix2.length}))
                `;
                break;
            }
            if (prefix2.length == 0) {
                break;
            }
            prefix2.pop();
        }

        f[id] = `
        //ac ${tag}
        
        fn ${fn(id)}(s:&str,offset:usize)->Option<(usize,usize)>{
            if offset < s.len() {
                match s.as_bytes()[offset] {
                    ${cases.join("")}
                    _ => {}
                }
            }
            ${returnStmt}
        }
        
        `
    }
    genIter(): string {
        return `
        #[allow(unused)]
        pub struct Iter<'a>(pub &'a str,pub usize);
        impl<'a> Iter<'a> {
            #[allow(unused)]
            #[inline]
            pub fn new(s:&'a str,last_index:usize)->Iter<'a>{
                Iter(s,last_index)
            }
        }
        impl core::iter::Iterator for Iter<'_> {
            type Item=[usize;2];
            #[allow(unused)]
            #[inline]
            fn next(&mut self) -> Option<[usize;2]> {
                match exec(self.0,self.1)[0] {
                    Some([start,end])=>{
                        if (self.1==end) {
                            self.1+=(UTF8_CHAR_WIDTH[*self.0.as_bytes().get(self.1).unwrap_or(&1) as usize] as usize)
                        }
                        else {
                            self.1=end;
                        }
                        Some([start,end])
                    }
                    None=>{
                        None
                    }
                }
            }
        }
        #[allow(unused)]
        pub struct IterStr<'a>(pub &'a str,pub usize);
        impl<'a> IterStr<'a> {
            #[allow(unused)]
            #[inline]
            pub fn new(s:&'a str,last_index:usize)->IterStr<'a>{
                IterStr(s,last_index)
            }
        }
        impl<'a> core::iter::Iterator for IterStr<'a> {
            type Item=&'a str;
            #[allow(unused)]
            #[inline]
            fn next(&mut self) -> Option<&'a str> {
                match exec(self.0,self.1)[0] {
                    Some([start,end])=>{
                        if (self.1==end) {
                            self.1+=(UTF8_CHAR_WIDTH[*self.0.as_bytes().get(self.1).unwrap_or(&1) as usize] as usize)
                        }
                        else {
                            self.1=end;
                        }
                        Some(&self.0[start..end])
                    }
                    None=>{
                        None
                    }
                }
            }
        }
        `
    }
    genTrie(alts: AST.Alternative[], f: string[], global: Global, id: number, next: number | void): boolean {
        if (global.l2r == false) {
            return false;
        }
        let trie = buildTrie(alts, this.ast.flags);
        if (trie == null) {
            return false;
        }
        return this.genSubTrie(trie, f, global, id, next);
    }
    genSubTrie(trie: Trie, f: string[], global: Global, id: number, next: number | void,): boolean {
        let prefix = [];
        while (trie.power == 0 && Object.keys(trie.subtries).length == 1) {
            prefix.push(Object.keys(trie.subtries)[0]);
            trie = trie.subtries[Object.keys(trie.subtries)[0]];
        }
        if (Object.keys(trie.subtries).length == 0) {
            let cases = ``;
            if (prefix.length > 0) {
                cases = `
            if !s.as_bytes()[*offset..].starts_with(&[${prefix.toString()}]) {
                return false
            }
            else {
                *offset+=${prefix.length};
            }
                `;
            }
            let fname = fn(id);
            f[id] = `
        ///trie
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            ${cases}
            return ${nextstmt(next)};
        }
`;
            return true;
        }

        let c = [];
        let subs = trieSubTrees(trie);
        for (let [nextids, subtrie] of subs) {
            let nextid = nextids.join("|");
            if (subtrie.power == 0) {
                let fid = newf(f);
                this.genSubTrie(subtrie, f, global, fid, null);
                c.push(`
                    ${nextid} => {
                        *offset+=1;
                        if ${fn(fid)}(s,offset,matches) {
                            ${nextstmt(next)}
                        }
                        else {
                            *offset-=1;
                            false
                        }
                    }`);
            }
            else {
                let fid = newf(f);
                this.genSubTrie(subtrie, f, global, fid, null);
                c.push(`
                    ${nextid} => {
                        *offset+=1;
                        if ${fn(fid)}(s,offset,matches) {
                            ${nextstmt(next)}
                        }
                        else {
                            ${nextstmt(next)}
                        }
                    }`);
            }
        }
        let cases = "";
        if (prefix.length > 0) {
            cases = `
            if !s.as_bytes()[*offset..].starts_with(&[${prefix.toString()}]) {
                return false;
            }            
            else {
                *offset+=${prefix.length};
            }
            `;
        }
        if (trie.power > 0) {
            let fname = fn(id);
            f[id] = `
        ///trie
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            ${cases}
            if *offset >= s.len() {
                return true;
            }
            match s.as_bytes()[*offset] {
${c.join("")}
                _ => {
                    ${nextstmt(next)}
                },
            }
        }
`;

            return true;
        }
        let fname = fn(id);
        f[id] = `
        ///trie
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            ${cases}
            if *offset >= s.len() {
                return false
            }
            match s.as_bytes()[*offset] {
${c.join("")}
                _ => false,
            }
        }
`
        return true;
    }
    genAlts(alts: AST.Alternative[], f: string[], global: Global, id: number, next: number | void) {
        if (this.genTrie(alts, f, global, id, next)) {
            return
        }
        let s: string[] = [];
        let raws = [];
        for (let alt of alts) {
            raws.push(alt.raw);
            let fid = newf(f);
            this.genAlt(alt, f, global, fid, null);
            s.push(
                `
            if ${fn(fid)}(s,offset,matches) {
                return ${nextstmt(next)};
            };
`);
        }
        let fname = fn(id);
        f[id] = `
        ///${raws.join("|")} Alternatives
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
${s.join("")}
            false
        }
`
    }

    genAlt(alt: AST.Alternative, f: string[], global: Global, id: number, next: number | void) {
        let eles = alt.elements;
        if (eles.length == 0) {
            let fname = fn(id);
            f[id] = `
            ///${alt.raw} Alternative
            #[inline]
            fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
                ${nextstmt(next)}
            }
    `
            return;
        }
        if (global.l2r == false) {
            eles = eles.reverse();
        }
        let fids = eles.map(() => newf(f));
        for (let eleid in eles) {
            this.genEle(eles[eleid], f, global, fids[eleid], fids[parseInt(eleid) + 1]);
        }

        let fname = fn(id);
        f[id] = `
        ///${alt.raw} Alternative
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            let offset2 = *offset;
            let matches2 = *matches;
            if !${fn(fids[0])}(s,offset,matches) {
                *offset=offset2;
                *matches=matches2;
                return false;
            }
            ${nextstmt(next)}
        }
`
    }
    genEle(ele: AST.Element, f: string[], global: Global, id: number, next: number | void) {
        if (ele.type == "Assertion") {
            return this.genAssertion(ele, f, global, id, next);
        }
        if (ele.type == "Character") {
            return this.genCharacter(ele, f, global, id, next);
        }
        if (ele.type == "CharacterClass") {
            return this.genCharacterClass(ele, f, global, id, next);
        }
        if (ele.type == "Backreference") {
            return this.genBackreference(ele, f, global, id, next);
        }
        if (ele.type == "CapturingGroup") {
            return this.genCapturingGroup(ele, f, global, id, next);
        }
        if (ele.type == "CharacterSet") {
            return this.genCharacterSet(ele, f, global, id, next);
        }
        if (ele.type == "Group") {
            return this.genGroup(ele, f, global, id, next);
        }
        if (ele.type == "Quantifier") {
            return this.genQuantifier(ele, f, global, id, next);
        }
    }
    genGroup(ele: AST.Group, f: string[], global: Global, id: number, next: number | void) {
        let altsid = newf(f);
        this.genAlts(ele.alternatives, f, global, altsid, null);
        f[id] = `
        ///${ele.raw} Group
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            if ${fn(altsid)}(s,offset,matches) {
                return ${nextstmt(next)};
            }
            false
        }
        `
    }
    genQuantifier(ele: AST.Quantifier, f: string[], global: Global, id: number, next: number | void) {
        if (ele.greedy) {
            return this.genQuantifierGreedy(ele, f, global, id, next);
        }
        let max = ele.max;
        let min = ele.min;
        let checkmax = "";
        if (Number.POSITIVE_INFINITY != max) {
            checkmax = `
                if count > ${max} {
                    *offset = offset2;
                    return false
                }`;
        }
        let fid = newf(f);
        this.genEle(ele.element, f, global, fid, null);

        let trynext = `
                        return true;
`;
        if (typeof next == "number") {
            trynext = `
                    if ${fn(next)}(s,offset,matches) {
                        return true;
                    }
`
        }

        f[id] = `
        ///${ele.raw} Quantifier
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            let mut offset2 = *offset;
            let mut count : usize = 0;
            loop {
                if count >= ${min} {
                    ${trynext}
                }
                if !${fn(fid)}(s,offset,matches) {
                    *offset = offset2;
                    return false;
                }
                count+=1;
${checkmax}
            }

        }
        `
    }
    genQuantifierGreedy(ele: AST.Quantifier, f: string[], global: Global, id: number, next: number | void) {
        let max = ele.max;
        let min = ele.min;
        let checkmax = "";
        if (Number.POSITIVE_INFINITY != max) {
            checkmax = `(n < ${max}) &&`;
        }
        let fid = newf(f);
        this.genEle(ele.element, f, global, fid, null);

        let trynext = `
                *offset = offset2;
                *matches = matches2;
                return true;
`;
        if (typeof next == "number") {
            trynext = `
                if ${fn(next)}(s,&mut offset2,&mut matches2) {
                    *offset = offset2;
                    *matches = matches2;
                    return true;
                }
                else {
                    return false;
                }
`
        }
        let minstmt = `
                if n < ${min} {
                    return false;
                }
`;
        if ( min == 0 ) {
            minstmt = "";
        }
        f[id] = `
        ///${ele.raw} QuantifierGreedy
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            fn search${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}],mut n:usize)->bool{
                let mut offset2 = *offset;
                let mut matches2 = *matches;
                if ${checkmax} ${fn(fid)}(s,&mut offset2,&mut matches2) {
                    n+=1;
                    if search${fn(id)}(s,&mut offset2,&mut matches2,n) {
                        *offset = offset2;
                        *matches = matches2;
                        return true;
                    }
                }
                ${minstmt}
${trynext}
            }
            search${fn(id)}(s,offset,matches,0)
        }
`
    }
    genCharacterSet(ele: AST.AnyCharacterSet | AST.EscapeCharacterSet | AST.UnicodePropertyCharacterSet, f: string[], global: Global, id: number, next: number | void) {
        return CharRanges.fromCharacterSet(ele, this.ast.flags.dotAll).gen(ele.raw, f, global, this.ast.flags.ignoreCase, this.count, id, next, true);
    }
    genCapturingGroup(ele: AST.CapturingGroup, f: string[], global: Global, id: number, next: number | void) {
        let groupid = this.offset2id[ele.start];
        let altsid = newf(f);
        this.genAlts(ele.alternatives, f, global, altsid, null);
        let range = global.l2r ? "start,end" : "end,start";
        f[id] = `
        ///${ele.raw} CapturingGroup
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            let start = *offset;
            if ${fn(altsid)}(s,offset,matches) {
                let end = *offset;
                matches[${groupid}]=Some([${range}]);
                return ${nextstmt(next)};
            }
            false
        }
        `
    }
    genBackreference(ele: AST.Backreference, f: string[], global: Global, id: number, next: number | void) {
        let refid: number;
        if (typeof ele.ref == "string") {
            refid = this.group2id[ele.ref];
            if (typeof refid != "number") {
                throw "bug";
            }
        }
        else {
            refid = ele.ref;
        }
        let cmp: string;
        let setnewoffset: string;
        if (global.l2r) {
            cmp = "s[*offset..].starts_with(matched)";
            setnewoffset = "*offset+=len;";
        }
        else {
            cmp = "s[..*offset].ends_with(matched)";
            setnewoffset = "*offset-=len;";
        }
        f[id] = `
        ///${ele.raw} Backreference
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            let refid = ${refid};
            match matches[refid]{
                Some([start,end])=>{
                    let matched = &s[start..end];
                    let len = matched.len();
                    if ${cmp} {
                        ${setnewoffset}
                        true
                    }
                    else {
                        false
                    }
                }
                None=>false,
            }
        }
        `
    }
    genCharacterClass(ele: AST.CharacterClass, f: string[], global: Global, id: number, next: number | void) {
        return CharRanges.fromCharacterClass(ele, this.ast.flags.dotAll).gen(ele.raw, f, global, this.ast.flags.ignoreCase, this.count, id, next, true);
    }
    genCharacter(ele: AST.Character, f: string[], global: Global, id: number, next: number | void) {
        return CharRanges.fromCharacter(ele).gen(ele.raw, f, global, this.ast.flags.ignoreCase, this.count, id, next, true);
    }
    genAssertion(ele: AST.Assertion, f: string[], global: Global, id: number, next: number | void) {
        if (ele.kind == "start") {
            return this.genStartAssertion(ele, f, global, id, next);
        }
        if (ele.kind == "end") {
            return this.genEndAssertion(ele, f, global, id, next);
        }
        if (ele.kind == "lookahead") {
            return this.genLookahead(ele, f, global, id, next);
        }
        if (ele.kind == "lookbehind") {
            return this.genLookbehind(ele, f, global, id, next);
        }
        if (ele.kind == "word") {
            return this.genWordBoundary(ele, f, global, id, next);
        }
    }
    genWordBoundary(ele: AST.WordBoundaryAssertion, f: string[], global: Global, id: number, next: number | void) {
        let wordRanges = CharRanges.word();
        let wordCheckPrevID = newf(f);
        wordRanges.gen("wordCheckPrev", f, Object.assign({}, global, { l2r: false }), this.ast.flags.ignoreCase, this.count, wordCheckPrevID, null, false);
        let wordCheckNextID = newf(f);
        wordRanges.gen("wordCheckNext", f, Object.assign({}, global, { l2r: true }), this.ast.flags.ignoreCase, this.count, wordCheckNextID, null, false);

        f[id] = `
        ///${ele.raw} WordBoundary
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            let prev_is_space = (*offset==0) || !${fn(wordCheckPrevID)}(s,offset,matches);
            let next_is_space = (s.len()==*offset) || !${fn(wordCheckNextID)}(s,offset,matches);
            if prev_is_space ${ele.negate ? "==" : "!="} next_is_space {
                return ${nextstmt(next)};
            }
            false
        }
        `
    }
    genLookbehind(ele: AST.LookbehindAssertion, f: string[], global: Global, id: number, next: number | void) {
        let altsid = newf(f);
        this.genAlts(ele.alternatives, f, Object.assign({}, global, { l2r: false }), altsid, null);
        f[id] = `
        ///${ele.raw} Lookbehind
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            let mut offset2 = *offset;
            let mut matches2 = *matches;
            if ${ele.negate ? "!" : ""}${fn(altsid)}(s,&mut offset2,&mut matches2) {
                *matches = matches2;
                return ${nextstmt(next)};
            }
            false
        }
        `
    }
    genLookahead(ele: AST.LookaheadAssertion, f: string[], global: Global, id: number, next: number | void) {
        let altsid = newf(f);
        this.genAlts(ele.alternatives, f, Object.assign({}, global, { l2r: true }), altsid, null);
        f[id] = `
        ///${ele.raw} Lookahead
        #[inline]
        fn ${fn(id)}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            let mut offset2 = *offset;
            let mut matches2 = *matches;
            if ${ele.negate ? "!" : ""}${fn(altsid)}(s,&mut offset2,&mut matches2) {
                *matches = matches2;
                return ${nextstmt(next)};
            }
            false
        }
        `
    }
    genStartAssertion(ele: AST.EdgeAssertion, f: string[], __: Global, id: number, next: number | void) {

        let multiline = "";
        if (this.ast.flags.multiline) {
            let lineTerminator = CharRanges.lineTerminator();
            let lineTerminatorCheckPrevID = newf(f);
            lineTerminator.gen("lineTerminatorCheckPrev", f, Object.assign({}, global, { l2r: false }), this.ast.flags.ignoreCase, this.count, lineTerminatorCheckPrevID, null, false);
            multiline = ` || ${fn(lineTerminatorCheckPrevID)}(s,offset,matches) `;
        }
        let fname = fn(id);
        f[id] =
            `
        ///${ele.raw} StartAssertion
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            if *offset == 0 ${multiline} {
                ${nextstmt(next)}
            }
            else {
                false
            }
            
        }
`
    }
    genEndAssertion(ele: AST.EdgeAssertion, f: string[], __: Global, id: number, next: number | void) {
        let multiline = "";
        if (this.ast.flags.multiline) {
            let lineTerminator = CharRanges.lineTerminator();
            let lineTerminatorCheckNextID = newf(f);
            lineTerminator.gen("lineTerminatorCheckNext", f, Object.assign({}, global, { l2r: true }), this.ast.flags.ignoreCase, this.count, lineTerminatorCheckNextID, null, false);
            multiline = ` || ${fn(lineTerminatorCheckNextID)}(s,offset,matches) `;
        }
        let fname = fn(id);
        f[id] =
            `
        ///${ele.raw} EndAssertion
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${this.count + 1}])->bool{
            if *offset == s.len() ${multiline} {
                ${nextstmt(next)}
            }
            else {
                false
            }
        }
`
    }
}


function buildCapturingGroupIDS(tree: AST.Node): [Record<number, number>, Record<string, number>, number] {
    let list: number[] = [];
    let group2offset: Record<string, number> = {};
    let n = 0;
    visitRegExpAST(tree, {
        onCapturingGroupEnter: (node) => {
            list.push(node.start);
            if (node.name) {
                group2offset[node.name] = node.start;
            }
            n++;
        }
    });
    list.sort((a, b) => a - b);
    let offset2id: Record<number, number> = {};
    for (let id in list) {
        offset2id[list[id]] = parseInt(id) + 1;
    }
    let group2id: Record<string, number> = {};

    for (let key of Object.keys(group2offset)) {
        group2id[key] = offset2id[group2offset[key]];
    }
    return [offset2id, group2id, n];
}

function buildGroupEnum(group2id: Record<string, number>): string {
    let s: string[] = [];
    for (let name of Object.keys(group2id)) {
        s.push(`            ${name} = ${group2id[name]},`)
    }
    return s.join("\n")
}


class CharRanges {
    static MAX = 0x10FFFF;
    static MIN = 0;
    ranges: [number, number][]
    constructor(ranges: [number, number][]) {
        this.ranges = ranges;
    }
    count(): number {
        let countValue = 0;
        for (let [start, end] of this.ranges) {
            countValue = countValue + end - start + 1;
        }
        return countValue;
    }
    charCodeList(): number[] {
        let list = [];
        for (let [start, end] of this.ranges) {
            for (let i = start; i <= end; i++) {
                list.push(i);
            }
        }
        return list;
    }
    static empty(): CharRanges {
        return new CharRanges([]);
    }
    static word(): CharRanges {
        return new CharRanges([[0x30, 0x39], [0x41, 0x5A], [0x5f, 0x5f], [0x61, 0x7A]]);
    }
    static fromElement(ele: AST.Element, dotall: boolean): CharRanges | null {
        if (ele.type == "Character") {
            return CharRanges.fromCharacter(ele);
        }
        else if (ele.type == "CharacterClass") {
            return CharRanges.fromCharacterClass(ele, dotall);
        }
        else if (ele.type == "CharacterSet") {
            return CharRanges.fromCharacterSet(ele, dotall);
        }
        return null;
    }
    static fromCharacterClass(ele: AST.CharacterClass, dotall: boolean): CharRanges {
        let base = this.empty();
        for (let ee of ele.elements) {
            if (ee.type == "Character") {
                base.union(CharRanges.fromCharacter(ee));
            } else if (ee.type == "CharacterSet") {
                base.union(CharRanges.fromCharacterSet(ee, dotall));
            } else if (ee.type == "CharacterClassRange") {
                base.union(CharRanges.fromCharacterClassRange(ee));
            }
        }
        if (ele.negate) {
            base.negate();
        }
        return base;
    }
    negate() {
        this.invert();
    }
    invert() {
        let ranges = this.ranges;
        let newRanges: [number, number][] = [];
        if (ranges.length == 0) {
            newRanges.push([CharRanges.MIN, CharRanges.MAX]);
            this.ranges = newRanges;
            return;
        }
        if (ranges[0][0] != 0) {
            newRanges.push([CharRanges.MIN, ranges[0][0] - 1])
        }
        for (let i = 0, j = 1; j < ranges.length; i++, j++) {
            newRanges.push([ranges[i][1] + 1, ranges[j][0] - 1]);
        }
        let last = ranges[ranges.length - 1];
        if (last[1] < CharRanges.MAX) {
            newRanges.push([last[1] + 1, CharRanges.MAX]);
        }
        this.ranges = newRanges;
    }
    intersect(another: CharRanges) {
        let lr = this.ranges;
        let rr = another.ranges;
        let result: [number, number][] = [];
        for (let l = 0, r = 0; (l < lr.length) && (r < rr.length);) {
            let lv = lr[l];
            let rv = rr[r];
            let start = Math.max(lv[0], rv[0]);
            let end: number;
            if (lv[1] < rv[1]) {
                end = lv[1];
                l++;
            }
            else {
                end = rv[1];
                r++;
            }
            if (start > end) {
                continue;
            }
            let cur: [number, number] = [start, end];
            if (result.length == 0) {
                result.push(cur);
                continue;
            }
            let prev = result[result.length - 1];
            if (prev[1] < (cur[0] - 1)) {
                result.push(cur);
                continue;
            }
            prev[1] = Math.max(prev[1], cur[1]);
        }
        this.ranges = result;
    }
    static fromCharacterClassRange(ee: AST.CharacterClassRange): CharRanges {
        return new CharRanges([[ee.min.value, ee.max.value]]);
    }
    union(another: CharRanges) {
        this.ranges = this.ranges.concat(another.ranges);
        this.reorganize();
    }
    static fromCharacterSet(ee: AST.AnyCharacterSet | AST.EscapeCharacterSet | AST.UnicodePropertyCharacterSet, dotall: boolean): CharRanges {
        let ranges: CharRanges;
        if (ee.kind == "digit") {
            ranges = new CharRanges([["0".codePointAt(0) as number, "9".charCodeAt(0) as number]]);
        } else if (ee.kind == "space") {
            ranges = CharRanges.whiteSpace();
            ranges.union(CharRanges.lineTerminator());
        } else if (ee.kind == "word") {
            ranges = CharRanges.word();
        } else if (ee.kind == "property") {
            ranges = CharRanges.property(ee.key, ee.value);
        } else if (ee.kind == "any") {
            if (dotall) {
                ranges = CharRanges.empty();
            }
            else {
                ranges = CharRanges.lineTerminator();
            }
            ranges.negate();
            return ranges;
        }
        if (ee.negate) {
            ranges.negate();
        }
        return ranges;
    }
    static property(propertyName: string, propertyValue: string | void): CharRanges {
        let key: string;
        propertyName = propertyAliases.get(propertyName) || propertyName;
        if (typeof propertyValue !== "string") {
            propertyAliases.get(propertyName) || propertyName
            key = `${propertyName}`
        }
        else {
            propertyValue = propertyValueAliases.get(propertyName).get(propertyValue) || propertyValue;
            key = `${propertyName}=${propertyValue}`
        }
        if (unicode[key] === undefined) {
            throw "is a bug";
        }
        return new CharRanges(unicode[key]);

    }
    static lineTerminator(): CharRanges {
        return new CharRanges([[0x000A, 0x000A], [0x000D, 0x000D], [0x2028, 0x2029]])
    }
    static whiteSpace(): CharRanges {
        let ws = new CharRanges([[0x0009, 0x0009], [0x000B, 0x000C], [0xFEFF, 0xFEFF]]);
        ws.union(CharRanges.property("General_Category", "Space_Separator"))
        return ws;
    }
    static fromCharacter(ele: AST.Character): CharRanges {
        return new CharRanges([[ele.value, ele.value]]);
    }
    private reorganize() {
        if (this.ranges.length == 0) {
            return;
        }
        this.ranges.sort((a, b) => a[0] - b[0]);
        let newranges = [this.ranges[0]];
        for (let item of this.ranges) {
            if (item[1] < item[0]) {
                throw new Error("bad input");
            }
            if ((newranges[newranges.length - 1][1] + 1) >= item[0]) {
                // cross
                newranges[newranges.length - 1][1] = Math.max(newranges[newranges.length - 1][1], item[1]);
            } else {
                newranges.push(item);
            }
        }
        this.ranges = newranges;
    }
    static ascii(): CharRanges {
        return new CharRanges([[0, 127]]);
    }
    static notAscii(): CharRanges {
        let ascii = new CharRanges([[0, 127]]);
        ascii.invert();
        return ascii;
    }
    isEmpty() {
        return this.ranges.length == 0;
    }
    onlyOne() {
        return (this.ranges.length == 1) && (this.ranges[0][0] == this.ranges[0][1]);
    }
    clone(): CharRanges {
        let ranges: [number, number][] = [];
        for (let item of this.ranges) {
            ranges.push([item[0], item[1]]);
        }
        return new CharRanges(ranges);
    }
    onlyAscii(): boolean {
        let cloned = this.clone();
        cloned.intersect(CharRanges.notAscii());
        return cloned.isEmpty();
    }
    unCaseFold() {
        const maybefold = new CharRanges(Array.from(fold.keys()).map(key => [key, key]));
        maybefold.reorganize();
        maybefold.intersect(this);
        let folded = [];
        for (let [start, end] of maybefold.ranges) {
            for (let item = start; item <= end; item++) {
                let vals = fold.get(item);
                for (let val of vals) {
                    folded.push([val, val]);
                }
            }
        }
        let ranges = new CharRanges(folded);
        ranges.reorganize();
        this.union(ranges);
    }
    gen(raw: string, f: string[], global: Global, ignoreCase: boolean, count: number, id: number, next: number | void, changeoffset: boolean) {
        if (ignoreCase) {
            this.unCaseFold();
        }
        if (this.isEmpty()) {
            return this.genEmpty(raw, f, global, count, id, next, changeoffset);
        }
        if (this.onlyOne()) {
            return this.genOne(raw, f, global, count, id, next, changeoffset);
        }
        if (this.onlyAscii()) {
            return this.genAscii(raw, f, global, count, id, next, changeoffset);
        }
        return this.genUnicode(raw, f, global, count, id, next, changeoffset);
    }
    genOne(raw: string, f: string[], global: Global, count: number, id: number, next: number | void, changeoffset: boolean) {

        let fname = fn(id);
        let cmp = global.l2r ? (
            `
            if s[*offset..].starts_with(CH) {
                ${changeoffset ? "*offset = *offset + LEN;" : ""}
                ${nextstmt(next)}
            }
            else {
                false
            }
`
        ) : (
            `
            if s[0..*offset].ends_with(CH) {
                ${changeoffset ? "*offset = *offset - LEN;" : ""}
                ${nextstmt(next)}
            }
            else {
                false
            }
`
        );
        f[id] =
            `
        ///${raw} OneChar
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${count + 1}])->bool{
            const CH : &'static str = "\\u{${this.ranges[0][0].toString(16)}}";
            const LEN : usize = CH.len();
            ${cmp}
        }
`
    }
    genEmpty(raw: string, f: string[], global: Global, count: number, id: number, next: number | void, changeoffset: boolean) {
        let fname = fn(id);
        f[id] =
            `
        ///${raw} EmptyChar
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${count + 1}])->bool{
            false
        }
`
    }
    genAscii(raw: string, f: string[], global: Global, count: number, id: number, next: number | void, changeoffset: boolean) {

        let fname = fn(id);
        let okCase = this.genCase();
        let cmp = global.l2r ? (
            `
            if *offset >= s.len() {
                return false
            }
            match b[*offset] {
                ${okCase}=>{
                    ${changeoffset ? "*offset = *offset + 1;" : ""}
                    ${nextstmt(next)}
                }
                _ => {
                    false
                }
            }
`
        ) : (
            `
            if *offset<1 {
                return false;
            }
            match b[*offset-1] {
                ${okCase}=>{
                    ${changeoffset ? "*offset = *offset - 1;" : ""}
                    ${nextstmt(next)}
                }
                _ => {
                    false
                }
            }
`
        );
        f[id] =
            `
        ///${raw} AsciiChar
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${count + 1}])->bool{
            let b = s.as_bytes();
            ${cmp}
        }
`
    }
    genUnicode(raw: string, f: string[], global: Global, count: number, id: number, next: number | void, changeoffset: boolean) {
        let fname = fn(id);
        let okCase = this.genCase();
        let cmp = global.l2r ? (
            `
            if *offset >= s.len() {
                return false
            }
            let ch = s[*offset..].chars().next().unwrap();
            match ch as u32 {
                ${okCase}=>{
                    ${changeoffset ? "*offset = *offset + ch.len_utf8();" : ""}
                    ${nextstmt(next)}
                }
                _ => {
                    false
                }
            }
`
        ) : (
            `
            if *offset<1 {
                return false;
            }
            let ch = s[0..*offset].chars().next_back().unwrap();
            match ch as u32 {
                ${okCase}=>{
                    ${changeoffset ? "*offset = *offset - ch.len_utf8();" : ""}
                    ${nextstmt(next)}
                }
                _ => {
                    false
                }
            }
`
        );
        f[id] =
            `
        ///${raw} Unicode Char
        #[allow(unused)]
        #[inline]
        fn ${fname}(s:&str,offset:&mut usize,matches:&mut [Option<[usize;2]>;${count + 1}])->bool{
            ${cmp}
        }
`
    }
    genCase(): string {
        let s = [];
        this.ranges.forEach(([start, end]) => {
            s.push(`0x${start.toString(16)}..=0x${end.toString(16)}`)
        })
        return s.join("|\n                ");
    }
}

function fn(id: number): string {
    return `f${id}`;
}
function newf(f: string[]): number {
    f.push("");
    return f.length - 1;
}
function newfWithCache(f: string[], prefix: number[], cache: Record<string, number>): [number, boolean] {
    let index = prefix.join(",");
    let notCache = false;
    if (cache[index] == undefined) {
        notCache = true;
        cache[index] = newf(f);
    }

    return [cache[index], notCache];
}
function nextstmt(next: number | void): string {
    return (typeof next == "number") ? `${fn(next)}(s,offset,matches)` : "true";
}

type Trie = {
    subtries: Record<number, Trie>,
    power: number,
    failPtr?: number[],
}

type TrieGenGlobal = {
    powerValue: number,
}

function buildTrie(alts: AST.Alternative[], flags: AST.Flags): Trie | null {
    let trie: Trie = {
        subtries: {},
        power: 0,
    }
    let global: TrieGenGlobal = {
        powerValue: 1,
    };
    if (buildTrieAlts(alts, flags, [], trie, global) == false) {
        return null;
    }
    removeDeathNode(trie, Infinity);
    return trie;
}

function buildTrieWithFail(alts: AST.Alternative[], flags: AST.Flags): Trie | null {
    let trie = buildTrie(alts, flags);
    if (trie == null) {
        return null;
    }
    trie.failPtr = [];
    buildFailPtr(trie, trie, []);
    //console.log(JSON.stringify(trie));
    return trie;
}

function buildTrieAlts(alts: AST.Alternative[], flags: AST.Flags, prefix: number[], trie: Trie, global: TrieGenGlobal,countnode?:[number]): boolean {
    countnode = countnode ?? [0];
    if (countnode[0]>100000) {
        return false;
    }
    countnode[0] ++;
    for (let alt of alts) {
        if (buildTrieAlt(alt.elements, flags, prefix, trie, global,countnode) == false) {
            return false;
        }
    }
    return true;
}

function buildTrieAlt(eles: AST.Element[], flags: AST.Flags, prefix: number[], trie: Trie, global: TrieGenGlobal,countnode:[number]): boolean {
    if (countnode[0]>1000) {
        return false;
    }
    countnode[0] ++;
    if (eles.length == 0) {
        insertTrie(trie, prefix, global);
        return true;
    }
    let ele = eles[0];
    let range = CharRanges.fromElement(ele, flags.dotAll);
    if (range != null) {
        if (flags.ignoreCase) {
            range.unCaseFold();
        }
        if (range.count() > 1000) {
            return false;
        }
        let charcodelist = range.charCodeList();
        for (let charCode of charcodelist) {
            if (buildTrieAlt(eles.slice(1), flags, prefix.concat(...charCode2utf8(charCode)), trie, global,countnode) == false) {
                return false;
            }
        }
    }
    else if (ele.type == "Group") {
        if (buildTrieAlts(ele.alternatives, flags, prefix, trie, global,countnode)) {
            return true;
        }
        else {
            return false;
        }
    }
    else if (ele.type=="Quantifier") {
        if (ele.max>20) {
            return false;
        }
        
        if (ele.greedy) {
            for(let i=ele.max;i>=ele.min;i--) {
                if (!buildTrieAlt(new Array(i).fill(ele.element).concat(eles.slice(1)), flags, prefix, trie, global,countnode)) {
                    return false;
                }
            }
        }
        else {
            for(let i=ele.min;i<=ele.max;i++) {
                if (!buildTrieAlt(new Array(i).fill(ele.element).concat(eles.slice(1)), flags, prefix, trie, global,countnode)) {
                    return false;
                }
            }
        }
        return true;

    }
    else {
        return false;
    }
    return true;
}

function charCode2utf8(charcode: number): Uint8Array {
    let s = String.fromCharCode(charcode);
    let len = utf8parser.length(s);
    let a = new Uint8Array(len);
    utf8parser.write(s, a, 0);
    return a;
}

function insertTrie(trie: Trie, prefix: number[], global: TrieGenGlobal) {
    for (let byte of prefix) {
        if (trie.subtries[byte] == undefined) {
            trie.subtries[byte] = {
                subtries: {},
                power: 0,
            }
        }
        trie = trie.subtries[byte];
    }
    if (trie.power == 0) {
        trie.power = global.powerValue;
        global.powerValue = global.powerValue + 1;
    }
    else {
        trie.power = Math.min(trie.power, global.powerValue);
        global.powerValue = global.powerValue + 1;
    }
}
function buildFailPtr(trie: Trie, root: Trie, prefix: number[]) {
    let subkeys = Object.keys(trie.subtries);
    let next: [Trie, number][] = [];
    for (let key of subkeys) {
        let subtrie = trie.subtries[key];
        next.push([subtrie, parseInt(key)]);
        if (getSubTrie(root, trie.failPtr).subtries[key] == subtrie) {
            subtrie.failPtr = [];
            continue;
        }
        if (getSubTrie(root, trie.failPtr).subtries[key] == undefined) {
            subtrie.failPtr = [];
            continue;
        }
        subtrie.failPtr = trie.failPtr.concat(parseInt(key));
        if (prefix.slice(0, subtrie.failPtr.length).every((v, i) => v == subtrie.failPtr[i])) {
            subtrie.failPtr = [];
        }
    }
    for (let [trie, key] of next) {
        buildFailPtr(trie, root, prefix.concat(key));
    }
}

function getSubTrie(root: Trie, path: number[]) {
    for (let s of path) {
        root = root.subtries[s];
    }
    return root;
}

function removeDeathNode(trie: Trie, prefixMin: number) {
    for (let key in trie.subtries) {
        let subtrie = trie.subtries[key];
        if ((subtrie.power != 0) && (prefixMin < subtrie.power)) {
            delete trie.subtries[key];
        }
        else {
            removeDeathNode(subtrie, subtrie.power == 0 ? prefixMin : Math.min(subtrie.power, prefixMin));
        }
        if (subtrie.power == 0 && Object.keys(subtrie.subtries).length == 0) {
            delete trie.subtries[key];
        }
    }
    if (trie.power > 0) {
        trie.power = 1;
    }
}

function trieSubTrees(trie: Trie): [number[], Trie][] {
    let records: Record<string, number[]> = {};
    for (let n in trie.subtries) {
        let s = JSON.stringify(trie.subtries[n]);
        if (records[s] == undefined) {
            records[s] = [];
        }
        records[s].push(parseInt(n));
    }
    return Object.entries(records).map(value => [value[1], trie.subtries[value[1][0]]]);
}

function minLen(ac: Trie, deep: number): number {
    let min = Number.MAX_SAFE_INTEGER;
    for (let next in ac.subtries) {
        min = Math.min(minLen(ac.subtries[next], deep + 1), min);
    }
    if (ac.power == 1) {
        min = Math.min(min, deep);
    }
    return min;
}

function pats(ac: Trie, prefix: number[]): number[][] {
    let l: number[][] = [];
    for (let next in ac.subtries) {
        l = l.concat(pats(ac.subtries[next], prefix.concat(parseInt(next))));
    }
    if (ac.power == 1) {
        l.push(prefix)
    }
    return l;
}

