// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import fs, { readFileSync, writeFileSync } from "fs";
import RegExp2Rust from "./index.js";
import child_process from 'child_process';

//let reg = /abc(def)(?<=def)(?=123)(?<!dee)(?!122)(?<Name>123)\k<Name>/u;
let reg = /^Sherlock Holmes|Sherlock Holmes$/umg;
//let regexstr= `.*`;
let program = new RegExp2Rust(reg).build();
let s = readFileSync("regexp2rust_macro/benches/sherlock.txt").toString("utf-8");
//let s = ``;
let matcheds=Array.from(s.matchAll(reg)).map((m,i)=>JSON.stringify(`${i},${m[0]}`));
writeFileSync("output2.json",`[${matcheds.join(",")}]`);
program = `fn main(){
    ${program}
    let s = r#"${s}"#;
    let result0 : Vec<_> = Iter(s,0).enumerate().map(|(i,n)|{
        format!("{},0x{:x}..0x{:x},'{}'",i,n[0],n[1],s.get(n[0]..n[1]).unwrap())
    }).collect();
    std::fs::write("output.json",format!("{:?}",result0)).unwrap();
}`
fs.writeFileSync('./example/src/main.rs', program, 'utf8');
let result = child_process.spawn('cargo', ['run', '-p', 'example', "--bin", "example"]);

result.stdout.on('data', (data) => {
    console.log(`${data}`);
});

result.stderr.on('data', (data) => {
    console.error(`${data}`);
});

result.on('close', (code) => {
    console.error(`child process exited with code ${code}`);
});