import { AST } from "regexpp";
type Global = {
    l2r: boolean;
};
export default class RegExp2Rust {
    ast: AST.RegExpLiteral;
    pattern: AST.Pattern;
    offset2id: Record<number, number>;
    group2id: Record<string, number>;
    groupenumstr: string;
    count: number;
    constructor(regex: RegExp);
    build(): string;
    genMemchr(enableMemchr: number[], alts: AST.Alternative[], f: string[]): [string, AST.Alternative[]];
    genAC(alts: AST.Alternative[], f: string[]): string | null;
    genSubAC(ac: Trie, f: string[], id: number, prefix: number[], root: Trie, tag: string, cache: Record<string, number>, prefixlen: number): void;
    genIter(): string;
    genTrie(alts: AST.Alternative[], f: string[], global: Global, id: number, next: number | void): boolean;
    genSubTrie(trie: Trie, f: string[], global: Global, id: number, next: number | void): boolean;
    genAlts(alts: AST.Alternative[], f: string[], global: Global, id: number, next: number | void): void;
    genAlt(alt: AST.Alternative, f: string[], global: Global, id: number, next: number | void): void;
    genEle(ele: AST.Element, f: string[], global: Global, id: number, next: number | void): void;
    genGroup(ele: AST.Group, f: string[], global: Global, id: number, next: number | void): void;
    genQuantifier(ele: AST.Quantifier, f: string[], global: Global, id: number, next: number | void): void;
    genQuantifierGreedy(ele: AST.Quantifier, f: string[], global: Global, id: number, next: number | void): void;
    genCharacterSet(ele: AST.AnyCharacterSet | AST.EscapeCharacterSet | AST.UnicodePropertyCharacterSet, f: string[], global: Global, id: number, next: number | void): void;
    genCapturingGroup(ele: AST.CapturingGroup, f: string[], global: Global, id: number, next: number | void): void;
    genBackreference(ele: AST.Backreference, f: string[], global: Global, id: number, next: number | void): void;
    genCharacterClass(ele: AST.CharacterClass, f: string[], global: Global, id: number, next: number | void): void;
    genCharacter(ele: AST.Character, f: string[], global: Global, id: number, next: number | void): void;
    genAssertion(ele: AST.Assertion, f: string[], global: Global, id: number, next: number | void): void;
    genWordBoundary(ele: AST.WordBoundaryAssertion, f: string[], global: Global, id: number, next: number | void): void;
    genLookbehind(ele: AST.LookbehindAssertion, f: string[], global: Global, id: number, next: number | void): void;
    genLookahead(ele: AST.LookaheadAssertion, f: string[], global: Global, id: number, next: number | void): void;
    genStartAssertion(ele: AST.EdgeAssertion, f: string[], __: Global, id: number, next: number | void): void;
    genEndAssertion(ele: AST.EdgeAssertion, f: string[], __: Global, id: number, next: number | void): void;
}
type Trie = {
    subtries: Record<number, Trie>;
    power: number;
    failPtr?: number[];
};

//# sourceMappingURL=index.d.ts.map
