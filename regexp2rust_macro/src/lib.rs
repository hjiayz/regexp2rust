#[doc = include_str!("../README.md")]
use core::panic;
use proc_macro::{TokenStream, TokenTree};
use std::{borrow::Cow, str::FromStr};

#[proc_macro]
pub fn regex2rust(input: TokenStream) -> proc_macro::TokenStream {
    let mut iter = input.into_iter();
    let name = match iter.next() {
        Some(TokenTree::Ident(ident)) => ident.to_string(),
        _ => {
            panic!("except function name.");
        }
    };
    match iter.next() {
        Some(TokenTree::Punct(eq)) if eq.to_string() == "=" => (),
        _ => {
            panic!("except =.");
        }
    };
    let regex = match iter.next() {
        Some(TokenTree::Literal(lit)) => lit.to_string(),
        Some(_) => panic!("except string literal for regexp."),
        None => panic!("miss regexp"),
    };
    let flags = match iter.next() {
        Some(TokenTree::Literal(lit)) => {
            let litstr = lit.to_string();
            if (!litstr.starts_with("\"")) || (!litstr.ends_with("\"")) {
                panic!("miss flags");
            }
            let s = litstr.get(1..(litstr.len() - 1)).expect("miss flags");
            for ch in s.chars() {
                if !['u', 'm', 's', 'i', 'g', 'y'].contains(&ch) {
                    panic!("bad flag:{}", ch);
                }
            }
            if litstr.find("u").is_none() {
                panic!("flags missing unicode(u)");
            }
            litstr
        }
        Some(_) => panic!("except string literal."),
        None => panic!("miss flags"),
    };
    proc_macro::TokenStream::from_str(&run(&regex, &flags, &name)).expect("to token faild")
}

fn run(regex: &str, flags: &str, name: &str) -> String {
    let mut regex2 = Cow::Borrowed(regex);
    if regex.starts_with("r\"") {
        regex2 = Cow::Owned(format!("/{}/.source", &regex[2..regex.len() - 1]));
    }
    const JS: &'static str = include_str!("simple.js");
    let code = format!(
        "
        {JS}
        globalThis.build({},{});
        ",
        regex2, flags
    );
    let result = jsexec::runjs(&code).expect("run failed");
    let result = if result.starts_with("1\n") {
        result.strip_prefix("1\n").unwrap()
    } else {
        if let Some(e) = result.strip_prefix("0\n") {
            panic!("{}", e);
        }
        else {
            panic!("{}", result);
        }
    };
    let out = format!("pub mod {} {{ {} }}", name, result);
    return out;
}
