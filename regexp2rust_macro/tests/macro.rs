// Copyright 2022 hjiayz
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
use regexp2rust_macro::regex2rust;
#[test]
fn run() {
    regex2rust!( foo = "12345(?<abc>123)" "ui");
    assert_eq!(foo::exec(" 12345123"), [Some([1, 9]), Some([6, 9])]);
    assert_eq!(foo::Group::abc as usize, 1usize);
    assert_eq!(
        foo::typed_matches(" 12345123").get_range(foo::Group::abc),
        Some([6, 9])
    );
    assert_eq!(
        foo::typed_matches(" 12345123").get_str(foo::Group::abc),
        Some("123")
    );
    assert_eq!(
        foo::typed_matches(" 12345123").get_range_id(1),
        Some([6, 9])
    );
    assert_eq!(foo::typed_matches(" 12345123").get_str_id(1), Some("123"));
    assert_eq!(foo::matches(" 12345123"), [Some("12345123"), Some("123")]);
}

#[test]
fn testend() {
    regex2rust!( foo = "$" "ui");
    assert_eq!(foo::exec(""), [Some([0, 0])]);
}

#[test]
fn testncgroup() {
    regex2rust!( foo = "(?:123)" "ui");
    assert_eq!(foo::exec("123"), [Some([0, 3])]);
}

#[test]
fn testcgroup() {
    regex2rust!( foo = "(123)1" "ui");
    assert_eq!(foo::exec("1231"), [Some([0, 4]), Some([0, 3])]);
}

#[test]
fn back_ref() {
    regex2rust!( foo = r"(?<abc>123)\k<abc>" "ui");
    assert_eq!(foo::exec(" 123123"), [Some([1, 7]), Some([1, 4])]);
}

#[test]
fn assertion() {
    regex2rust!( foo = r"(?<=123)123" "ui");
    assert_eq!(foo::exec(" 123123"), [Some([4, 7])]);
    regex2rust!( foo2 = r"121(?=123)" "ui");
    assert_eq!(foo2::exec(" 121123"), [Some([1, 4])]);
    regex2rust!( foonot = r"121(?<!121)123" "ui");
    assert_eq!(foonot::exec(" 121123"), [None]);
    regex2rust!( foo2not = r"121(?!123)" "ui");
    assert_eq!(foo2not::exec(" 121123"), [None]);
    regex2rust!( start = r"^121" "ui");
    assert_eq!(start::exec(" 121"), [None]);
    regex2rust!( start2 = r"^121" "ui");
    assert_eq!(start2::exec("121 "), [Some([0, 3])]);
    regex2rust!( word = r"\bxxx" "ui");
    assert_eq!(word::exec("xxx "), [Some([0, 3])]);
    assert_eq!(word::exec(" xxx "), [Some([1, 4])]);
    regex2rust!( word2 = r"\B121" "ui");
    assert_eq!(word2::exec("121 "), [None]);
    assert_eq!(word2::exec("2121 "), [Some([1, 4])]);
}

#[test]
fn global() {
    regex2rust!( foo = "1" "uig");
    assert_eq!(foo::exec("111", 1), [Some([1, 2])]);
    let x: Vec<[usize; 2]> = foo::Iter::new("111", 0).collect();
    assert_eq!(x, [[0, 1], [1, 2], [2, 3]]);
    regex2rust!( anymore = ".*" "uig");
    assert_eq!(anymore::Iter("123\n", 1).collect::<Vec<_>>(), [[1, 3],[3,3],[4,4]]);
}

#[test]
fn sticky() {
    regex2rust!( foo = "1" "uiy");
    assert_eq!(foo::exec(" 111", 1), [Some([1, 2])]);
    assert_eq!(foo::exec(" 111", 0), [None]);
    assert_eq!(foo::matches(" 111", 0), [None]);
}

#[test]
fn globalsticky() {
    regex2rust!( foo = "1" "uigy");
    let x: Vec<[usize; 2]> = foo::Iter::new("11 1", 0).collect();
    assert_eq!(x, [[0, 1], [1, 2]]);
    let x: Vec<&str> = foo::IterStr::new("11 1", 0).collect();
    assert_eq!(x, ["1", "1"]);
}

#[test]
fn dotall() {
    regex2rust!( foo = "." "us");
    assert_eq!(foo::exec("\n"), [Some([0, 1])]);
    regex2rust!( foon = "." "u");
    assert_eq!(foon::exec("\n"), [None]);
}

#[test]
fn multiline() {
    regex2rust!( foo = "^1" "um");
    assert_eq!(foo::exec("\n1"), [Some([1, 2])]);
    regex2rust!( foon = "1$" "um");
    assert_eq!(foon::exec("1\n"), [Some([0, 1])]);
}

#[test]
fn test_memchr() {
    regex2rust!( foo = "[cgt]gggtaaa|tttaccc[acg]" "ug");
    assert_eq!(foo::exec("tttacccc", 0), [Some([0, 8])]);
    regex2rust!( foo2 = "[cgt]gggtaaa|[cgt]ttaccc[acg]" "ug");
    assert_eq!(foo2::exec("tttacccc", 0), [Some([0, 8])]);
    regex2rust!( foo3 = "Sher[a-z]+|Hol[a-z]+" "uig");
    assert_eq!(foo3::exec("hole", 0), [Some([0, 4])]);
}

#[test]
fn test_ac() {
    regex2rust!( foo = "abcdef|bc" "ug");
    assert_eq!(foo::exec("abcde", 0), [Some([1, 3])]);
}

#[test]
fn test_ac2() {
    regex2rust!( foo = "tttaccca|tttaa" "ug");
    assert_eq!(foo::exec("ttttaaattt", 0), [Some([1, 6])]);
}

#[test]
fn test_ac_quantifier() {
    regex2rust!( foo = "x{0,7}" "ug");
    assert_eq!(foo::exec("xxxx", 0), [Some([0, 4])]);
}

#[test]
fn test_word() {
    regex2rust!( foo = r"\b\w+n\b" "ug");
    assert_eq!(foo::exec("in", 0), [Some([0, 2])]);
}


#[test]
fn test_repeated_class_negation() {
    regex2rust!( foo = r"[a-q][^u-z]{13}x" "ug");
    assert_eq!(foo::exec("is clear. For ex", 0), [None]);
    assert_eq!(foo::exec("ndation are tax", 0), [Some([0,15])]);
}